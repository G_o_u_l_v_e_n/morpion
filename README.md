# Lancer et installer l'application Morpion

## Installer l'application

Pour installer l'application merci de lancer la commande :
### `npm install`

## Lancer l'application

Pour lancer l'application merci de lancer la commande :
### `npm start`

Puis dans votre navigateur préféré ouvrez l'url : [http://localhost:3000](http://localhost:3000).
