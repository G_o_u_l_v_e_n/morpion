import React from 'react';
import { useState } from 'react';
import "../App.css";

function Game() {
  const [board, setBoard] = useState([
    ['', '', ''],
    ['', '', ''],
    ['', '', '']
  ]);
  const [currentPlayer, setCurrentPlayer] = useState('X');
  const [winner, setWinner] = useState(null);

  function checkForWinner(currentPlayer) {
    const boardGame = board;

    for (let i = 0; i < 3; i++) {
      let gameBoard = boardGame[i];
      if ((gameBoard[0] === gameBoard[1] && gameBoard[0] === gameBoard[2] && (gameBoard[0] === "O" || gameBoard[0] === "X") && winner === null)) {
      setWinner(currentPlayer);
      }
    }

    for (let i = 0; i < 3; i++) {
      if ((boardGame[0][i] === boardGame[1][i] && boardGame[0][i] === boardGame[2][i] && (boardGame[0][i] === "X" || boardGame[0][i] === "O") && winner === null)) {
      setWinner(currentPlayer);
      }
    }
    if ((boardGame[0][0] === boardGame[1][1] && boardGame[0][0] === boardGame[2][2] && (boardGame[0][0] === "X" || boardGame[0][0] === "O") && winner === null)) {
      setWinner(currentPlayer);

    } else if ((boardGame[0][2] === boardGame[1][1] &&boardGame[0][2] === boardGame[2][0] &&(boardGame[0][2] === "X" || boardGame[0][2] === "O") && winner === null)) {
      setWinner(currentPlayer);

    } else {
      console.log("Suite de la partie");
    }
  }

  function handleClick(rowIndex, colIndex, row, col) {
    // console.log(row + " : " + col);
    if(row !== 'X' && row !== 'O' && col !== 'X' && col !== 'O') {
      const newBoard = [...board];
      newBoard[rowIndex][colIndex] = currentPlayer;
      setBoard(newBoard);
      setCurrentPlayer(currentPlayer === 'X' ? 'O' : 'X');
      checkForWinner(currentPlayer);
    }
  }

  function reloadGame() {
    // window.location.reload(false)
    setWinner(null);
    setBoard([
    ['', '', ''],
    ['', '', ''],
    ['', '', '']]);
  }

  return (
    <div>
        <h2>Mon super morpion</h2>
        <table>
          <tbody>
            {board.map((row, rowIndex) => (
              <tr key={rowIndex}>
                {row.map((col, colIndex) => (
                  <td key={colIndex} onClick={() => handleClick(rowIndex, colIndex, row, col)}>
                    {col}
                  </td>
                ))}
              </tr>
            ))}
          </tbody>
        </table>
        <p className='button' onClick={() => reloadGame()}> Cliquer ici pour relancer la partie</p>

        {winner ? (
        <p>Le joueur {winner} a gagné !</p>
      ) : ""}
    </div>
  );
}

export default Game;
